import sys
from xml.dom import minidom

if len(sys.argv) != 3:
  print('\nUsage: python results-to-csv.py <source-file>.lem.xml <destination-file>\n')
  sys.exit(1)
  
INPUT_FILE = sys.argv[1]
OUTPUT_FILE = sys.argv[2]

# Unlock geoparser .lem.xml file
xmldoc = minidom.parse(INPUT_FILE)

ents = xmldoc.getElementsByTagName('ents') 

with open(OUTPUT_FILE, 'w') as out:

  out.write("Transcription;Type;Gazetteer ID;Gazetteer ID;lat;long;\n")

  for entityList in ents:
    if entityList.attributes['source'].value == 'ner-rb':
      for entity in entityList.getElementsByTagName('ent'):
      
        # Named entity type
        line = entity.attributes['type'].value + ';'
      
        # Gazetteer reference (if any)
        if entity.hasAttribute('gazref'):
          line += entity.attributes['gazref'].value + ';'
        else:
          line += ';'
          
        # Source gazetteer reference (if any)
        if entity.hasAttribute('source-gazref'):
          line += entity.attributes['source-gazref'].value + ';'
        else:
          line += ';'
        
        # Lat/long (if any)
        if entity.hasAttribute('lat') and entity.hasAttribute('long'):
          line += entity.attributes['lat'].value + ';'
          line += entity.attributes['long'].value + ';'
        else:
          line += ';;'
        
        # Entity
        for part in entity.getElementsByTagName('part'):
          partLine = part.firstChild.nodeValue + ';' + line
          out.write((partLine + '\n').encode('utf8'))
          
  out.close
