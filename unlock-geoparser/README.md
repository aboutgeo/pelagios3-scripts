# Unlock Geoparser Scripts

Some scripts to work with the [Unlock Text](http://edina.ac.uk/unlock/texts/) Geoparsing service. Note: some
scripts require the Python [Requests](http://www.python-requests.org/) module. Install the module using
_pip install requests_.

## Configuration

Before you can use the scripts, you need to configure your user credentials with Unlock. Enter the folder
``config``. Create a copy of the file ``unlock-user.json.template`` named ``unlock-user.json``, and enter
your Unlock username and key in this file. __IMPORTANT: Do create a copy, and don't just rename the file!
Otherwise you risk overwriting the file in the repository, and your user credentials will end up on
GitHub.__

## Scripts

* __submit-job.py__ submits a new job to the Unlock Text service. Usage:
  ``python submit-job.py <job-id> <text-1-url> <text-2-url> ...``
* __get-status.py__ query the status of the specified job. Usage:
  ``python get-status <job-id>``
* __delete-job.py__ delete the specified job. Usage:
  ``python delete-job <job-id>``
* __download-results.py__: batch-download the results for the specified job. Usage:
  ``python download-results.py <job-id>``.
* __results-to-csv.py__ convert the .lem.xml result file to a CSV file for further processing. Usage:
  ``python results-to-csv.py <source-file>.lem.xml <destination-file>``

