import sys
import json
import requests

# Load credentials
if len(sys.argv) != 2:
  print('\nUsage: python get-status.py <job-id>\n')
  sys.exit(1)

credentials = ""
try:
  credentials = json.loads(open('./config/unlock-user.json', 'r').read())
except:
  print('\nMissing user config! Please create a file named \'unlock-user.json\' from the provided template.\n')
  sys.exit(1)  

UNLOCK_USER = credentials['username']
UNLOCK_KEY = credentials['key']
UNLOCK_BASE_URL = 'http://unlock.edina.ac.uk/text-api/'
UNLOCK_JOB_ID = sys.argv[1]

headers = { 'Accept': 'application/json', 'Authorization': UNLOCK_KEY }
response = requests.get(UNLOCK_BASE_URL + 'users/' + UNLOCK_USER + '/batchjobs/' + UNLOCK_JOB_ID, headers = headers)
  
print(response.text)
