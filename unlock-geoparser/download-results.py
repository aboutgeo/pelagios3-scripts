import os
import sys
import json
import urllib2

if len(sys.argv) != 2:
  print('\nUsage: python download-results.py <batchjob-id>\n')
  sys.exit(1)

# Load credentials
credentials = ""
try:
  credentials = json.loads(open('./config/unlock-user.json', 'r').read())
except:
  print('\nMissing user config! Please create a file named \'unlock-user.json\' from the provided template.\n')
  sys.exit(1)  

UNLOCK_USER = credentials['username']
UNLOCK_KEY = credentials['key']
UNLOCK_BASE_URL = 'http://unlock.edina.ac.uk/text-api/'
UNLOCK_JOB_ID = sys.argv[1]

def createRequest(url):
  request = urllib2.Request(url) 
  request.add_header('Accept', 'application/json')
  request.add_header('Authorization', UNLOCK_KEY)
  return request

def mapToFileURLs(text):
  files = []
  for url in text['output']:
    files.append(url)
  return (text['resource-id'], files)

# Get JSON metadata from Unlock
response = urllib2.urlopen(createRequest(UNLOCK_BASE_URL + '/users/' + UNLOCK_USER + '/batchjobs/' + UNLOCK_JOB_ID))
jobMetadata = json.loads(response.read())

# Extract file URLs from job metadata
filesets = map(mapToFileURLs, jobMetadata['Texts'])

# Write to folder
for (name, fileset) in filesets:
  print('Getting results for: ' + name)
  if not os.path.exists(name):
    os.makedirs(name)
    
  for url in fileset:
    print('Downloading file: ' + url)
    
    try:
      response = urllib2.urlopen(createRequest(url))
      with open(name + '/' + url.split('/')[-1], 'w') as out:
        out.write(response.read())
    except:
      print('An error occurred...')

  print('Results in folder /' + name)
      
