package org.pelagios.tools.peutinger

import scala.io.Source
import java.io.PrintWriter

object PeutingerTopology extends App {

  val ovToTPPlace = loadOVtoTPPlaceIdentityTable("data/ovplaces.php")
  
  val tpplaceToPleiades = loadTPPlaceToPleiadesIdentityTable("peutinger-for-recogito.csv")
  
  val connections = loadConnections("data/largeset.php")
  
  val csvWriter = new PrintWriter("peutinger-connections.csv")
  csvWriter.println("from;to;distance")
  connections.foreach { case (from, to, distance) => {
    val pleiadesFrom = ovToTPPlace.get(from).map(tpplaceToPleiades.get(_)).flatten
    val pleiadesTo = ovToTPPlace.get(to).map(tpplaceToPleiades.get(_)).flatten
    
    if (pleiadesFrom.isDefined && pleiadesTo.isDefined) {
      csvWriter.println(pleiadesFrom.get + ";" + pleiadesTo.get + ";" + distance)
    } else if (pleiadesFrom.isDefined) {
      println("WARNING: no Pleiades match for " + to)
    } else if (pleiadesTo.isDefined) {
      println("WARNING: no Pleiadews match for " + from)
    } else {
      println("WARNING: no Pleiades matches for " + from + " and " + to)
    }
  }}
  csvWriter.flush()
  csvWriter.close()
  
  def loadOVtoTPPlaceIdentityTable(php: String) = {
    Source.fromFile(php, "UTF-8").getLines
      .map(_.split("=>"))
      .map(array => {
        val ovID = array(0).trim
        val phpSrc = array(1)
        val startIdx = phpSrc.indexOf('"') + 1
        val tpplaceID = phpSrc.substring(startIdx, phpSrc.indexOf('"', startIdx)).trim
        (ovID, tpplaceID)
      }).toMap
  }
  
  def loadConnections(php: String) = {
    Source.fromFile(php, "UTF-8").getLines
      .filter(!_.trim.isEmpty)  // Remove empty lines
      .filter(!_.startsWith("#")) // Remove comment lines
      .filter(!_.startsWith("/*"))
      .map(line => { // Remove inline comments
        if (line.contains("#"))
          line.substring(0, line.indexOf('#')).trim
        else
          line
      })
      .map(php => {
        val triple = php.substring("array(".size, php.indexOf(')')).split(",")
        (triple(0).trim, triple(1).trim, triple(2).trim.toInt)
      })
  }
  
  def loadTPPlaceToPleiadesIdentityTable(csv: String) = {
    Source.fromFile(csv, "UTF-8").getLines
      .drop(1) // Drop column headers (FEATURE_ID,BATLAS_ID,PLEIADES_ID)
      .map(_.split(";", -1)) // Split fields
      .map(array => {
        val pleiadesURI = array(2)
        val sourceURI = array(4)
        val tpplaceId = sourceURI.substring(sourceURI.lastIndexOf('/') + 1, sourceURI.lastIndexOf('.'))
        // (array(0), tpplaceId, pleiadesURI)
        (tpplaceId, pleiadesURI)
      // }).filter(!_._3.isEmpty).toSeq.groupBy(_._2).filter(_._2.size > 1)
      }).filter(!_._2.isEmpty).toMap
  }

}