package org.pelagios.tools.peutinger

import scala.xml.{ XML, Node, Text }
import scala.io.Source
import java.io.PrintWriter
import org.openrdf.rio.rdfxml.RDFXMLParserFactory
import org.pelagios.legacy.rdf.parser.AnnotationCollector
import java.io.FileInputStream
import java.io.File

/** A utility to convert the Peutinger Table data to Recogito-compliant CSV.
  *
  * Recogito requires CSV according to the following format:
  * 
  * gdoc_part;toponym;gazetteer_uri;source;
  * 
  * where
  *   document_part = Peutinger Table segment name
  *   toponym = toponym 'name' from Peutinger Table data
  *   gazetteer_uri = corresponding Pleiades match
  *   source = HTML page URL for the name (based on the 'key' parameter in the PT data) 
  * 
  * @author Rainer Simon <rainer.simon@ait.ac.at>  
  */
object Converter extends App {
  
  private val BASE_PATH = "data/prm/"
    
  private val SOURCE_TEMPLATE = "http://www.cambridge.org/us/talbert/talbertdatabase/@@key@@.html"
    
  private val PLEIADES_PREFIX = "http://pleiades.stoa.org/places/"
    
  // Build BAtlas-to-Pleiades identity table
  println("Loading BAtlas-to-Pleiades identity table")
  val lookupTable = Source.fromFile("data/batlasids-2011.csv", "UTF-8").getLines
    .drop(1) // Drop column headers (FEATURE_ID,BATLAS_ID,PLEIADES_ID)
    .map(_.split(",")) // Split by comma
    .map(array => (array(1), array(2))).toMap // create a lookup table BAtlasID->PleiadesID  
  println("Done - " + lookupTable.size + " identities") 
  
  println("Loading OmnesVia TPPlace-to-Pleiades identity table")
  val omnesViae = buildOmnesViaeLookup
  
  // Parse grid index
  println("Parsing grid index")
  val segments = (XML.load(BASE_PATH + "indexgrid.xml") \ "div" \ "list" \ "item").map(new Segment(_))
  println("Done - " + segments.size + " segments, " + segments.flatMap(_.gridsquares).size + " gridsquares")
  
  val jsonWriter = new PrintWriter("peutinger-for-recogito.json")
  jsonWriter.println("{ \"parts\":[")
  
  val csvWriter = new PrintWriter("peutinger-for-recogito.csv")
  csvWriter.println("gdoc_part;toponym;gazetteer_uri;status;source;")
  
  // Process each grid segment...
  var pleiadesMatches = 0
  segments.foreach(seg => {
    println("Processing Segment '" + seg.name + "' (" + seg.gridsquares.size + " gridsquares)")
    seg.gridsquares.foreach {  case (gsquareName, id, foo) => {
      // Process each gridsquare
      println("  Processing gridsquare '" + id + "'")
      
      jsonWriter.println("  {\"title\":\"" + seg.name + " " + gsquareName + "\", \"source\":\"http://www.cambridge.org/us/talbert/talbertdatabase/" + id + ".html\"},")
      
      val gridsquare = new GridSquare(XML.load(BASE_PATH + id + ".xml"))
      gridsquare.items.foreach { case (name, key, description) => {
        // We need to pull the BAtlas ID from the XML file for this toponym
        val place = new TPPlace(XML.load(BASE_PATH + key + ".xml"))
        
        val gazetteerURI = {
          val omnesViaeMatch = omnesViae.get(key)
          if (omnesViaeMatch.isDefined) {
            omnesViaeMatch
          } else {
            lookupTable.get(place.bAtlasID).map(PLEIADES_PREFIX + _)
          }
        }
        
        val status = if (gazetteerURI.isDefined) {
          pleiadesMatches += 1
          "VERIFIED"
        } else {
          "NOT_VERIFIED"
        }
          
        val line = seg.name + " " + gsquareName + ";" + name.replace(";", "\\;").replace("\n", " ") + ";" + gazetteerURI.getOrElse("") + ";" + status + ";" + SOURCE_TEMPLATE.replace("@@key@@", key)
        if (key.equalsIgnoreCase("TPPlace98"))
          println(line)
        csvWriter.println(line)
      }};
    }}    
  })
  
  csvWriter.flush()
  csvWriter.close()
  
  jsonWriter.println("]}")
  jsonWriter.flush()
  jsonWriter.close()
  
  println("Done - " + pleiadesMatches + " matches to Pleiades")
  
  def buildOmnesViaeLookup(): Map[String, String] = {
    val parser = new RDFXMLParserFactory().getParser()
    val annotationCollector = new AnnotationCollector()
    parser.setRDFHandler(annotationCollector)
    parser.parse(new FileInputStream(new File("data/omnesviae.rdf")), "http://www.omnesvia.org/")

    val tpPlaceAnnotations = annotationCollector.getAnnotations.filter(_.target.uri.indexOf("TPPlace") > 0)
    tpPlaceAnnotations.map(a => {
      val tpID = a.target.uri.substring("http://omnesviae.org/api/sites/".size)
      (tpID, a.body)
    }).toMap
  }
  
}

class Segment(private val node: Node) {
  
  lazy val name = node.child.filter(_.isInstanceOf[Text]).headOption.map(_.toString.trim).getOrElse("")
  
  lazy val gridsquares = (node \ "list" \ "item").map(item => { 
    (item.text.toString.trim, (item \ "xref" \ "@doc").toString, (item \ "xref").text.trim)
  })
    
}

class GridSquare(private val node: Node) {
  
  lazy val items = (node \ "div" \ "list" \ "item").map(item => {
    val name = (item \ "name").text.trim
    val key = item \ "name" \ "@key" toString
    val description = item.child.filter(_.isInstanceOf[Text]).headOption.map(_.toString.trim).getOrElse("")
    (name, key, description)
  }).filter(!_._2.isEmpty) // Everything that has no key has no URI -> so we discard
  
}

class TPPlace(private val node: Node) {  
  
  private lazy val references = (node \ "div").filter(n => (n \ "@id").toString == "TPReferences").head \ "listBibl" \ "bibl"
  private lazy val batlasReference = references.filter(bibl => (bibl \ "title").text.trim == "BAtlas")
  
  lazy val bAtlasID = (batlasReference \ "biblScope").text.trim.toLowerCase().replace(" ", "-")
  
}