package org.pelagios.tools.wikidata

import java.io.PrintWriter
import scala.io.Source
import org.json4s._
import org.json4s.jackson.JsonMethods._
import org.hamcrest.core.IsInstanceOf

object PlaceJSON2CSV extends App {
  
  val freebaseLookup: Option[FreebaseLookup] = None // Some(new FreebaseLookup("data/fb2w.nt"))

  val PLACE_JSON = "data/geolocated-resources-json.txt"
    
  val namesCSV = new PrintWriter("data/names.csv")
  namesCSV.println("wikidata_id;language;name")
  
  val placesCSV = new PrintWriter("data/places.csv")
  placesCSV.println("wikidata_id;lat;lon;alt;instance_of;freebase_uri")
  
  val gazetteerRDF = new PelagiosGazetteerRDFWriter("data/wikidata.ttl")
  
  println("Processing file " + PLACE_JSON)
  
  val startTime = System.currentTimeMillis
  Source.fromFile(PLACE_JSON, "UTF-8").getLines.foreach(line => {
    try {
      // println(line)
      val json = parse(line)
    
      // Wikidata Q-ID
      val qid = {
        val entityField = (json \ "entity")
        if (entityField.isInstanceOf[JArray]) {
          entityField.asInstanceOf[JArray](1).asInstanceOf[JInt].num.toInt
        } else {
          val stringField = entityField.asInstanceOf[JString].values.toLowerCase().trim
          if (stringField.startsWith("q"))
            stringField.substring(1).toInt
          else
            stringField.toInt
        }
      }
    
      // Labels
      val labelFields = (json \ "label")
      val labels = 
        if (labelFields.isInstanceOf[JObject])    
          labelFields.values.asInstanceOf[Map[String, String]].toSeq
        else
          Seq.empty[(String, String)]
             
      // Aliases
      val aliasFields = (json \ "aliases")
      val aliases =
        if (aliasFields.isInstanceOf[JObject]) {
          val byLanguage = aliasFields.values.asInstanceOf[Map[String, Any]].toSeq
          byLanguage.flatMap { case (language, aliases) =>  {
            if (aliases.isInstanceOf[List[String]]) {
              aliases.asInstanceOf[List[String]].map(alias => (language, alias)) 
            } else if (aliases.isInstanceOf[Map[Int, String]]){
              aliases.asInstanceOf[Map[Int, String]].values.map(alias => (language, alias))
            } else {
              println(aliases)
              Seq.empty[(String, String)]
            }
          }}
        } else {
          Seq.empty[(String, String)]
        }
     
      val names = ((labels ++ aliases).distinct).map { case (language, name) => (language,
          name
            .replace(";", "\\;")
            .replace("\"", "\\\"")
      )}
      
      // Write names CSV
      names.foreach { case (lang, name) => namesCSV.println(qid + ";\"" + lang + "\";\"" + name + "\"") }
    
      // Wikidata 'claims'
      val claims = (json \ "claims").asInstanceOf[JArray].values.map(v => new Claim(v.asInstanceOf[Map[String, Any]]))
    
      // Geolocation claim
      val geoClaim = claims.filter(_.isGeoLocation)
      val geoLocation = geoClaim.head.getGeoLocation
      
      // instanceOf claims
      val instanceClaims = claims.filter(_.isType)
      val types = instanceClaims.flatMap(_.getTypes)
      
      // Freebase URI
      val freebaseURI = freebaseLookup.flatMap(_.getFreebaseURI(qid))
      
      // Write places CSV
      placesCSV.println(qid + ";" + geoLocation._1 + ";" + geoLocation._2 + ";" + geoLocation._3.getOrElse("") + ";" + types.mkString(",") + ";" + freebaseURI.getOrElse(""))
      
      // Write gazetteer RDF
      gazetteerRDF.writePlace(qid, None, names, geoLocation)
    } catch {
      case t: Throwable => {
        println(t.getMessage())
        println(line)
        t.printStackTrace
        throw new RuntimeException
      }
    }
   })
  
  namesCSV.flush()
  namesCSV.close()
  
  placesCSV.flush()
  placesCSV.close()
  
  gazetteerRDF.close()
  
  println("Done. Took " + (System.currentTimeMillis - startTime) + "ms")
  
}

class Claim(map: Map[String, Any]) {
  
  private val claimElements: List[List[Any]] = {    
    val m = map.get("m").get.asInstanceOf[List[Any]]    
    // May have further (double!) nested objects
    val q = map.get("q").get.asInstanceOf[List[List[Any]]]
    m +: q
  }
  
  private lazy val instanceOfStatements = claimElements.filter(statement => {
    (statement.size == 4) && 
    (statement(0).isInstanceOf[String] && (statement(0).asInstanceOf[String].equals("value"))) &&
    (statement(1).isInstanceOf[BigInt] && (statement(1).asInstanceOf[BigInt].toInt == 31))
  })
  
  def isGeoLocation(): Boolean = {
    claimElements.filter(element => element.contains("globecoordinate")).size > 0
  }
  
  def isType(): Boolean =
    instanceOfStatements.size > 0
  
  def getTypes(): Seq[Int] = {
    instanceOfStatements.map(statement => {
      val entity = statement(3).asInstanceOf[Map[String, Any]]
      entity.get("numeric-id").get.asInstanceOf[BigInt].toInt
    })    
  }
  
  def getGeoLocation(): (Double, Double, Option[Double]) = {
    val map = {
      val element = claimElements.filter(_.contains("globecoordinate")).head  
      element(3).asInstanceOf[Map[String, Any]]
    }
    
    val lat = map.get("latitude").asInstanceOf[Option[Double]]    
    val lon = map.get("longitude").asInstanceOf[Option[Double]]
    val alt = { 
      val opt = map.get("altitude").asInstanceOf[Option[Double]]
      if (opt.isDefined && (opt.get == null))
        None
      else
        opt
    }
    
    (lat.get, lon.get, alt)
  }
  
}