package org.pelagios.tools.wikidata

import scala.io.Source
import java.io.PrintWriter
import scala.xml.Utility
import org.apache.commons.lang.StringEscapeUtils
import scala.util.parsing.json.JSON

object Dump2PlaceJSON extends App {
  
  val XML_SNIPPETS = "/home/simonr/Downloads/wikidata-latest-pages-meta-jsonpayload.txt"
    
  val resultsWriter = new PrintWriter("results.txt")
    
  // Load the file, line by line
  val leadingXML = "<text xml:space=\"preserve\">"
  val leadingXMLSize = leadingXML.size
    
  Source.fromFile(XML_SNIPPETS, "UTF-8").getLines.foreach(line => {
    // If the file was correctly prepared, lines will always start with '<text xml:space="preserve">'
    // But there may be leading spaces before the XML snippet
    val lineWithoutStartTag = line.substring(line.indexOf(leadingXML) + leadingXMLSize)
    val cleanedLine =
      { if (lineWithoutStartTag.contains("</"))
          lineWithoutStartTag.substring(0, lineWithoutStartTag.indexOf("</"))
        else
          lineWithoutStartTag } trim()
    
    // This is kind of weird, but the quickest way considering how Scala XML handling works
    if (!cleanedLine.isEmpty) {
      val unescapedJSON = StringEscapeUtils.unescapeXml(cleanedLine)
      try {
        val parsed = JSON.parseRaw(unescapedJSON)
        if (parsed.isDefined) {
          if (unescapedJSON.contains("globecoordinate"))
            resultsWriter.println(unescapedJSON)
        }
      } catch {
        case t: Throwable => // Do nothing
      }
    }
  })
  
  resultsWriter.flush()
  resultsWriter.close()
  
}
