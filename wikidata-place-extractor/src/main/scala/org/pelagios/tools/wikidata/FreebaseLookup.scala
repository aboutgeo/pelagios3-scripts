package org.pelagios.tools.wikidata

import org.openrdf.rio.helpers.RDFHandlerBase
import org.openrdf.model.impl.ValueFactoryImpl
import org.openrdf.model.Statement
import org.mapdb.DBMaker
import org.openrdf.rio.RDFParserRegistry
import org.openrdf.rio.RDFFormat
import java.io.FileInputStream

class FreebaseLookup(file: String) {

  println("Parsing file: " + file)
  val startTime = System.currentTimeMillis
  val parser = RDFParserRegistry.getInstance.get(RDFFormat.N3).getParser
  val collector = new SameAsCollector()
  
  parser.setRDFHandler(collector)
  parser.parse(new FileInputStream(file), "http://freebase.org/")
  
  val links = collector.links
  println("Collected " + links.size + " Freebase-to-Wikidata links (took " + (System.currentTimeMillis - startTime) + "ms)")
  
  def getFreebaseURI(qid: Int): Option[String] =
    Option(links.get("http://www.wikidata.org/entity/Q" + qid))
  
}

class SameAsCollector extends RDFHandlerBase {
  
  private val sameAs = ValueFactoryImpl.getInstance.createURI("http://www.w3.org/2002/07/owl#", "sameAs")
  
  val links = DBMaker.newTempHashMap[String, String]
  
  override def handleStatement(statement: Statement) =
    if (statement.getPredicate == sameAs)
      links.put(statement.getObject.stringValue, statement.getSubject.stringValue)

}