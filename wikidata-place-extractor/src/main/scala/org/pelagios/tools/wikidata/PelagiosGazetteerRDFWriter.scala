package org.pelagios.tools.wikidata

import java.io.PrintWriter

class PelagiosGazetteerRDFWriter(file: String) {
  
  private val MAX_FILESIZE = 10000000
  
  private var placeCtr = 0
  
  private var fileCtr = 0

  private def unesc(str: String) =
    str.replace("\"", "").replace("\\", "")
  
  private var writer: PrintWriter = null
  startNewFile()
  
  def writePlace(qid: Int, description: Option[String], names: Seq[(String, String)], location: (Double, Double, Option[Double])) = {
    if (placeCtr > MAX_FILESIZE)
      startNewFile()
      
    if (names.size > 0) {
      writer.println("<http://www.wikidata.org/wiki/Q" + qid + "> a pelagios:PlaceRecord ;")
      writer.println("  dcterms:title \"" + unesc(names.head._2) + "\" ;")
      description.map(d =>
        writer.println("  dcterms:description \"" + d + "\" ;"))    
      names.foreach { case (language, name) => 
        writer.println("  pleiades:hasName [ rdfs:label \"" + unesc(name) + "\"@" + language + " ] ;") }
      writer.println("  pleiades:hasLocation [ geo:lat \"" + location._1 + "\"^^xsd:double ; geo:long \"" + location._2 + "\"^^xsd:double ] ;")
      writer.println("  .\n")
    }
    
    placeCtr += 1
  }
  
  private def startNewFile() {
    if (writer != null) 
      close()
      
    fileCtr += 1
    val filname = file.substring(0, file.lastIndexOf('.')) + "_" + fileCtr + ".ttl"
    writer = new PrintWriter(filname)
    placeCtr = 0
    
    writer.println("@prefix dcterms: <http://purl.org/dc/terms/> .")
    writer.println("@prefix osgeo: <http://data.ordnancesurvey.co.uk/ontology/geometry/> .")
    writer.println("@prefix pelagios: <http://pelagios.github.io/vocab/terms#> .")
    writer.println("@prefix pleiades: <http://pleiades.stoa.org/places/vocab#> .")
    writer.println("@prefix geo: <http://www.w3.org/2003/01/geo/wgs84_pos#> .")
    writer.println("@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .")
    writer.println("@prefix skos: <http://www.w3.org/2004/02/skos/core#> .")
    writer.println("@prefix spatial: <http://geovocab.org/spatial#> .")
    writer.println("@prefix gn: <http://www.geonames.org/ontology#> .")
    writer.println()    
  }
  
  def close() = {
    writer.flush()
    writer.close()
  }

}