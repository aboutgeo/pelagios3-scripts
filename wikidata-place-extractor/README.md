# Extracting Basic Place Data from Wikidata

This project contains some helpers to extract basic place data from Wikidata. Note that the result will 
contain __all__ things in Wikidata that have geo-coordinates. So additional filtering will be needed to
separate actual _places_ from other geo-located things.

## Step 1 - Grab Base Data from Wikidata

To start, download the appropriate Wikidata dump from their [Database Download](http://www.wikidata.org/wiki/Wikidata:Database_download) site.
The correct URL at the time of writing for this is [http://dumps.wikimedia.org/wikidatawiki/](http://dumps.wikimedia.org/wikidatawiki/).

Grab the latest dump named 'wikidatawiki-latest-pages-meta-current.xml.bz2'. (At the time of writing, this is approx. a 2.5GB download,
which uncompresses to about 35GB of data.)

## Step 2 - Prefiltering

The file contains lots of stuff we don't need. So it makes sense to discard discard all lines in the dump that don't contain
actual page data. On Unix, use the following command to keep only the relevant lines:

    grep "<text xml:space=\"preserve\">" wikidatawiki-latest-pages-meta-current.xml > wikidata-latest-pages-meta-jsonpayload.txt

## Step 3 - Extracting & Unescaping JSON Payload

The file `wikidata-latest-pages-meta-jsonpayload.txt` we have produced in step 2 now contains, in each line, an XML snippet
including the page contents. The page contents are JSON; within the XML snippet all special characters (including quotes)
are XML escaped. Run the Scala application `org.pelagios.tools.wikidata.Dump2PlaceJSON` to produce a cleaned-up file that contains the
unescaped JSON (one line per Wikidata record), filtered to __only__ those Wikidata records that contain geographical coordinates.

## Step 4 - Generating CSVs

The file produced in Step 3 nwo contains, in each line, a JSON representation of a single, geo-located Wikidata entity.
Run the scala application `org.pelagios.tools.wikidata.PlaceJSON2CSV` to generate two CSVs: one containing an entry
for each Wikidata entity (with lat, lon, altitude and instanceOf information); and one containing all names (labels
and aliases) for each Wikidata entity.



