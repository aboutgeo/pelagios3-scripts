name := "wikidata-place-extractor"

version := "0.0.1"

scalaVersion := "2.10.0"

/** Runtime dependencies **/
libraryDependencies ++= Seq(
  "commons-lang" % "commons-lang" % "2.6",
  "org.json4s" %% "json4s-jackson" % "3.2.9",
  "org.openrdf.sesame" % "sesame-rio-n3" % "2.7.5",
  "org.openrdf.sesame" % "sesame-rio-rdfxml" % "2.7.5",
  "org.mapdb" % "mapdb" % "0.9.0"
)

/** Test dependencies **/
libraryDependencies ++= Seq(
  "junit" % "junit" % "4.11" % "test",
  "org.scalatest" % "scalatest_2.10" % "1.9.1" % "test"
)
