package org.pelagios.tools.dbpedia

import java.io.{ FileInputStream, PrintWriter }
import org.mapdb.DBMaker
import org.openrdf.model.Statement
import org.openrdf.model.impl.ValueFactoryImpl
import org.openrdf.rio.{ RDFFormat, RDFParserRegistry }
import org.openrdf.rio.helpers.RDFHandlerBase
import scala.io.Source

object AddPopuplation extends App {
  // TODO make configurable
  val PATH_TO_INPUT_FILE = "/home/simonr/Arbeitsfläche/gazetteer-data/raw-dumps/places.csv"
  val PATH_TO_POPULATION_DUMP = "/home/simonr/Arbeitsfläche/gazetteer-data/raw-dumps/mappingbased_properties_en.nt"
  val PATH_TO_RESULT_FILE = "/home/simonr/Arbeitsfläche/gazetteer-data/raw-dumps/places_population.csv"
    
  // Step 1: parse coordinates file & build lookup table
  println("Parsing file: " + PATH_TO_POPULATION_DUMP)
  val startTime = System.currentTimeMillis
  val parser = RDFParserRegistry.getInstance.get(RDFFormat.N3).getParser
  val collector = new PopulationCollector()
  
  parser.setRDFHandler(collector)
  parser.parse(new FileInputStream(PATH_TO_POPULATION_DUMP), "http://dbpedia.org/")
  
  val pop = collector.population
  println("Collected " + pop.size + " DBpedia population stats (took " + (System.currentTimeMillis - startTime) + "ms)")

  // Step 2: parse input CSV and write output CSV - only include places where we have a Wikidata ID
  val outputCSV = new PrintWriter(PATH_TO_RESULT_FILE)
  outputCSV.println("geonames_uri;wikidata_uri;dbpedia_uri;place_types;lat;long;population_total;")
  
  val inputCSV = Source.fromFile(PATH_TO_INPUT_FILE).getLines
  val header = inputCSV.take(1).toSeq.head.split(";", -1)
  val dbpediaUriIdx = header.indexWhere(_.equals("dbpedia_uri"))
  
  var counter = 0
  inputCSV.map(_.split(";", -1)).foreach(fields => {
    val dbpediaURI = fields(dbpediaUriIdx)
    val populationTotal = pop.get(dbpediaURI)
    val csv = if (populationTotal != null) {
        counter += 1
        fields.take(fields.size -1) :+ populationTotal
      } else {
        fields.take(fields.size -1) :+ ""
      }
        
    outputCSV.println(csv.mkString(";"))
  })
  
  outputCSV.flush()
  outputCSV.close()
  
  println("Added " + counter + " population values")
  println("Done.")
}

class PopulationCollector extends RDFHandlerBase {
  
  private val POPULATION_TOTAL = ValueFactoryImpl.getInstance().createURI("http://dbpedia.org/ontology/", "populationTotal")
  
  val population = DBMaker.newTempHashMap[String, String]
  
  override def handleStatement(statement: Statement) =
    if (statement.getPredicate == POPULATION_TOTAL)
      population.put(statement.getSubject.stringValue, statement.getObject.stringValue) 
}