package org.pelagios.tools.dbpedia

import java.io.{ FileInputStream, PrintWriter }
import org.openrdf.rio.{ RDFFormat, RDFParserRegistry }
import org.openrdf.rio.helpers.RDFHandlerBase
import org.openrdf.model.Statement
import org.openrdf.model.impl.ValueFactoryImpl
import org.mapdb.DBMaker
import scala.io.Source

object AddNames extends App {
  // TODO make configurable
  val PATH_TO_INPUT_FILE = "/home/simonr/Arbeitsfläche/gazetteer-data/raw-dumps/names.csv"
  val PATH_TO_PROPERTIES_DUMP = "/home/simonr/Arbeitsfläche/gazetteer-data/raw-dumps/mappingbased_properties_en.nt"
  val PATH_TO_RESULT_FILE = "/home/simonr/Arbeitsfläche/gazetteer-data/raw-dumps/names_en.csv"
    
  // Step 1: parse coordinates file & build lookup table
  println("Parsing file: " + PATH_TO_PROPERTIES_DUMP)
  val startTime = System.currentTimeMillis
  val parser = RDFParserRegistry.getInstance.get(RDFFormat.N3).getParser
  val collector = new NamesCollector()
  
  parser.setRDFHandler(collector)
  parser.parse(new FileInputStream(PATH_TO_PROPERTIES_DUMP), "http://dbpedia.org/")
  
  val names = collector.names
  println("Collected " + names.size + " names (took " + (System.currentTimeMillis - startTime) + "ms)")

  // Step 2: parse input CSV and write output CSV
  val outputCSV = new PrintWriter(PATH_TO_RESULT_FILE)
  outputCSV.println("wikidata_uri;name;language;")
  
  val inputCSV = Source.fromFile(PATH_TO_INPUT_FILE).getLines
  val header = inputCSV.take(1).toSeq.head.split(";", -1)
  val dbpediaUriIdx = header.indexWhere(_.equals("dbpedia_uri"))
  val wikidataUriIdx = header.indexWhere(_.equals("wikidata_uri"))
  
  inputCSV.map(_.split(";", -1)).foreach(fields => {
    val dbpediaURI = fields(dbpediaUriIdx)
    val wikidataURI = fields(wikidataUriIdx)
    val nameString = names.get(dbpediaURI)
    if (nameString != null) {
      nameString.split(",").foreach(n => {
        outputCSV.println(wikidataURI + ";" + n + ";en;")      
      })
    }   
  })
  
  outputCSV.flush()
  outputCSV.close()
  
  println("Done.")
}

class NamesCollector extends RDFHandlerBase {
  
  private val FOAF_NAME = ValueFactoryImpl.getInstance().createURI("http://xmlns.com/foaf/0.1/", "name")
  
  val names = DBMaker.newTempHashMap[String, String]
  
  override def handleStatement(statement: Statement) =
    if (statement.getPredicate == FOAF_NAME) {
      val uri = statement.getSubject.stringValue
      val knownNames = names.get(uri)
      if (knownNames == null) {
        names.put(uri, statement.getObject.stringValue)
      } else {
        names.put(uri, knownNames + "," + statement.getObject.stringValue)
      }
    }
}