package org.pelagios.tools.dbpedia

import java.io.{ FileInputStream, PrintWriter }
import org.openrdf.model.{ Statement, URI }
import org.openrdf.model.impl.ValueFactoryImpl
import org.openrdf.rio.{ RDFFormat, RDFParserRegistry }
import org.openrdf.rio.helpers.RDFHandlerBase
import scala.collection.mutable.ListBuffer

/** Takes the DBpedia 'Mapping-based Types' dump file and produces a CSV with URIs of *only* the places **/
object ExtractPlaces extends App {
  
  // TODO make configurable
  val PATH_TO_TYPES_DUMP = "/home/simonr/Arbeitsfläche/gazetteer-data/raw-dumps/instance_types_en.ttl"
  val PATH_TO_RESULT_FILE = "/home/simonr/Arbeitsfläche/gazetteer-data/raw-dumps/places.csv"
    
  // Step 1: parse RDF file & collect all relevant rdf:type triples
  println("Parsing file: " + PATH_TO_TYPES_DUMP)
  val startTime = System.currentTimeMillis
  val parser = RDFParserRegistry.getInstance.get(RDFFormat.N3).getParser
  val collector = new PlaceTypeCollector()
  
  parser.setRDFHandler(collector)
  parser.parse(new FileInputStream(PATH_TO_TYPES_DUMP), "http://dbpedia.org/ontology/")
  println("Collected " + collector.flatTypes.size + " rdf:type triples (took " + (System.currentTimeMillis - startTime) + "ms)")
    
  // Step 2: group the triples, so that we end up with a map [resource -> list of types]
  println("Grouping results...")
  val aggregatedRelations = collector.groupedTypes

  // Step 3: write to file
  val printWriter = new PrintWriter(PATH_TO_RESULT_FILE)
  printWriter.println("dbpedia_uri;place_types;")
  aggregatedRelations.foreach { case (uri, types) => printWriter.println(uri + ";" + types.mkString(",") + ";") }
  printWriter.flush()
  printWriter.close()
  
  println("Done.")

}

/** RDF handler that filters the DBpedia 'Mapping-based Types' dump so that only place resources remain **/
class PlaceTypeCollector extends RDFHandlerBase {
  
  private val factory = ValueFactoryImpl.getInstance()
  
  private val RDF_TYPE = factory.createURI("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "type")
  
  private val ALLOWED_TYPES = 
    Set("HistoricPlace", "Mountain", "MountainPass", "NaturalPlace", "BodyOfWater", "Mountain",
      "MountainRange", "Valley", "Volcano", "PopulatedPlace", "Continent", "Settlement", "Island", "Country",
      "State", "Territory", "Region", "WorldHeritageSite")
    .map(term => factory.createURI("http://dbpedia.org/ontology/", term))
  
  val flatTypes = ListBuffer.empty[(String, String)]
  
  def groupedTypes = flatTypes.groupBy(_._1).mapValues(_.map(_._2).toSeq)
  
  override def handleStatement(statement: Statement) =
    if (statement.getPredicate == RDF_TYPE && ALLOWED_TYPES.contains(statement.getObject.asInstanceOf[URI]))
      flatTypes.append((statement.getSubject().stringValue, statement.getObject().stringValue))

}