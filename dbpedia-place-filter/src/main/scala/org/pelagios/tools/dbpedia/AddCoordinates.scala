package org.pelagios.tools.dbpedia

import java.io.{ FileInputStream, PrintWriter }
import org.mapdb.DBMaker
import org.openrdf.model.Statement
import org.openrdf.model.impl.ValueFactoryImpl
import org.openrdf.rio.{ RDFFormat, RDFParserRegistry }
import org.openrdf.rio.helpers.RDFHandlerBase
import scala.io.Source

/** Takes a 'places' CSV table and appends a geocoordinates column.
  *
  * Minimum requirement for the CSV is that it has a 'dbpedia_uri' column.    
  */
object AddCoordinates extends App {
  // TODO make configurable
  val PATH_TO_INPUT_FILE = "/home/simonr/Arbeitsfläche/gazetteer-data/raw-dumps/places.csv"
  val PATH_TO_COORDINATES_DUMP = "/home/simonr/Arbeitsfläche/gazetteer-data/raw-dumps/mappingbased_properties_en.nt"
  val PATH_TO_RESULT_FILE = "/home/simonr/Arbeitsfläche/gazetteer-data/raw-dumps/places_coordinates.csv"
    
  // Step 1: parse coordinates file & build lookup table
  println("Parsing file: " + PATH_TO_COORDINATES_DUMP)
  val startTime = System.currentTimeMillis
  val parser = RDFParserRegistry.getInstance.get(RDFFormat.N3).getParser
  val collector = new CoordinatesCollector()
  
  parser.setRDFHandler(collector)
  parser.parse(new FileInputStream(PATH_TO_COORDINATES_DUMP), "http://geonames.org/")
  
  val coords = collector.coords
  println("Collected " + coords.size + " DBpedia geo-coordinates (took " + (System.currentTimeMillis - startTime) + "ms)")

  // Step 2: parse input CSV and write output CSV - only include places where we have a Wikidata ID
  val outputCSV = new PrintWriter(PATH_TO_RESULT_FILE)
  outputCSV.println("geonames_uri;wikidata_uri;dbpedia_uri;place_types;lat;long;")
  
  val inputCSV = Source.fromFile(PATH_TO_INPUT_FILE).getLines
  val header = inputCSV.take(1).toSeq.head.split(";", -1)
  val dbpediaUriIdx = header.indexWhere(_.equals("dbpedia_uri"))
  
  inputCSV.map(_.split(";", -1)).foreach(fields => {
    val dbpediaURI = fields(dbpediaUriIdx)
    val coordString = coords.get(dbpediaURI)
    if (coordString != null) {
      val latLong = coordString.split(" ")
      val csv = (fields.take(fields.length - 1) :+ latLong(0)) :+ latLong(1)
      outputCSV.println(csv.mkString(";") + ";")
    }
  })
  
  outputCSV.flush()
  outputCSV.close()
  
  println("Done.")
}

/** RDF handler that collects DBpedia-to-GeoNames URI-mappings from an RDF dump file **/
class CoordinatesCollector extends RDFHandlerBase {
  
  private val GEORSS_POINT = ValueFactoryImpl.getInstance().createURI("http://www.georss.org/georss/", "point")
  
  val coords = DBMaker.newTempHashMap[String, String]
  
  override def handleStatement(statement: Statement) =
    if (statement.getPredicate == GEORSS_POINT)
      coords.put(statement.getSubject.stringValue, statement.getObject.stringValue)
  
}