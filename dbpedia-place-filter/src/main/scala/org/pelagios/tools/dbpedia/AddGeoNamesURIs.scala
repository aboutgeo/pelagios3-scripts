package org.pelagios.tools.dbpedia

import java.io.{ FileInputStream, PrintWriter }

import org.openrdf.rio.{ RDFFormat, RDFParserRegistry }
import org.openrdf.rio.helpers.RDFHandlerBase
import org.openrdf.model.Statement
import org.openrdf.model.impl.ValueFactoryImpl
import scala.collection.mutable.HashMap
import scala.io.Source
import org.mapdb.DBMaker

/** Takes a 'places' CSV table and adds a Wikidata Q-ID column.
  *
  * Minimum requirement for the CSV is that it has a 'dbpedia_uri' column.    
  */
object AddGeoNamesURIs extends App {  
  
  // TODO make configurable
  val PATH_TO_INPUT_FILE = "/home/simonr/Arbeitsfläche/gazetteer-data/raw-dumps/places_qid.csv"
  val PATH_TO_GEONAMES_LINKS_FILE = "/home/simonr/Arbeitsfläche/gazetteer-data/dbpedia_geonames_links.nt"
  val PATH_TO_RESULT_FILE = "/home/simonr/Arbeitsfläche/gazetteer-data/raw-dumps/places_qid_geonames.csv"
    
  // Step 1: parse link file & build mapping table
  println("Parsing file: " + PATH_TO_GEONAMES_LINKS_FILE)
  val startTime = System.currentTimeMillis
  val parser = RDFParserRegistry.getInstance.get(RDFFormat.N3).getParser
  val collector = new GeonamesLinkCollector()
  
  parser.setRDFHandler(collector)
  parser.parse(new FileInputStream(PATH_TO_GEONAMES_LINKS_FILE), "http://geonames.org/")
  
  val links = collector.links
  println("Collected " + links.size + " GeoNames-to-Wikidata links (took " + (System.currentTimeMillis - startTime) + "ms)")

  // Step 2: parse input CSV and write output CSV - only include places where we have a Wikidata ID
  val outputCSV = new PrintWriter(PATH_TO_RESULT_FILE)
  outputCSV.println("geonames_uri;wikidata_uri;dbpedia_uri;place_types;")
  
  val inputCSV = Source.fromFile(PATH_TO_INPUT_FILE).getLines
  val header = inputCSV.take(1).toSeq.head.split(";", -1)
  val dbpediaUriIdx = header.indexWhere(_.equals("dbpedia_uri"))
  
  inputCSV.map(_.split(";", -1)).foreach(fields => {
    val dbpediaURI = fields(dbpediaUriIdx)
    val geonamesURI = links.get(dbpediaURI)
    val csv = if (geonamesURI != null)
        geonamesURI +: fields
      else
        "" +: fields
        
    outputCSV.println(csv.mkString(";"))
  })
  
  outputCSV.flush()
  outputCSV.close()
  
  println("Done.")
}

/** RDF handler that collects DBpedia-to-GeoNames URI-mappings from an RDF dump file **/
class GeonamesLinkCollector extends RDFHandlerBase {
  
  private val SAME_AS = ValueFactoryImpl.getInstance().createURI("http://www.w3.org/2002/07/owl#", "sameAs")
  
  val links = DBMaker.newTempHashMap[String, String]
  
  override def handleStatement(statement: Statement) =
    if (statement.getPredicate == SAME_AS)
      links.put(statement.getSubject.stringValue, statement.getObject.stringValue)
  
}