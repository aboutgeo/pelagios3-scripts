# Pelagios 3 Scripts

Various scripts and helper utilties produced during the course of the
[Pelagios 3](http://pelagios-project.blogspot.co.uk) project.

* [unlock-geoparser](http://github.com/pelagios/pelagios3-scripts/tree/master/unlock-geoparser). Scripts to work 
  with the [Unlock Text](http://edina.ac.uk/unlock/texts/) Geoparsing service.
