# Wikipedia Category Flattener

A utility to build a flat list of all DBpedia category URIs 'underneath' a specific category. 

## Hacking 

The tool is written in Scala and built using SBT. Information for installing SBT is [here](http://www.scala-sbt.org/release/docs/Getting-Started/Setup.html).
(It's a download & unzip thing, though). You need Java 1.6 or 1.7 installed on your system.

To generate an Eclipse project type

    sbt eclipse

To run the tool type

    sbt run
