name := "wikipedia-category-flattener"

version := "0.0.1"

scalaVersion := "2.10.0"

/** Runtime dependencies **/
libraryDependencies ++= Seq(
   "org.slf4j" % "slf4j-simple" % "1.7.5",
  "org.openrdf.sesame" % "sesame-rio-n3" % "2.7.5",
  "org.openrdf.sesame" % "sesame-rio-rdfxml" % "2.7.5"
)

/** Test dependencies **/
libraryDependencies ++= Seq(
  "junit" % "junit" % "4.11" % "test",
  "org.scalatest" % "scalatest_2.10" % "1.9.1" % "test"
)
