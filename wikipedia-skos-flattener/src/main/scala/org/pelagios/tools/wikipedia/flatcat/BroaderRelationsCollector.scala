package org.pelagios.tools.wikipedia.flatcat

import org.openrdf.model.Statement
import org.openrdf.model.impl.ValueFactoryImpl
import org.openrdf.rio.helpers.RDFHandlerBase
import scala.collection.mutable.ListBuffer

class BroaderRelationsCollector extends RDFHandlerBase {
  
  private val SKOS_BROADER = ValueFactoryImpl.getInstance.createURI("http://www.w3.org/2004/02/skos/core#", "broader")
  
  val collectedRelations = ListBuffer.empty[(String, String)]
  
  def aggregatedRelations = collectedRelations.groupBy(_._2).mapValues(_.map(_._1).toSeq)
  
  override def handleStatement(statement: Statement) = {
    if (statement.getPredicate == SKOS_BROADER) {
      collectedRelations.append((statement.getSubject().stringValue, statement.getObject().stringValue))
    }
  }

}