package org.pelagios.tools.wikipedia.flatcat

import org.openrdf.rio.{ RDFFormat, RDFParserRegistry }
import java.io.FileInputStream

object CategoryFlattener extends App {
  
  val PATH_TO_SKOS_FILE = "/home/simonr/Arbeitsfläche/gazetteer-data/raw-dumps/skos_categories_en.nt"
    
  val START_URI = "http://dbpedia.org/resource/Category:Populated_places"
    
  // Step 1: parse SKOS file & collect all skos:broader relations into a HashMap
  println("Parsing " + PATH_TO_SKOS_FILE)
  val startTime = System.currentTimeMillis
  val parser = RDFParserRegistry.getInstance.get(RDFFormat.N3).getParser
  val collector = new BroaderRelationsCollector()
  
  parser.setRDFHandler(collector)
  parser.parse(new FileInputStream(PATH_TO_SKOS_FILE), "http://dbpedia.org/ontology/")
  
  println("Collected " + collector.collectedRelations.size + " skos:broader relations (took " + (System.currentTimeMillis - startTime) + "ms)")
    
  // Step 2: collect all category URIs that have the start URI defined as their "broader"
  println("Grouping results...")
  val aggregatedRelations = collector.aggregatedRelations
  def getNarrowerTerms(broaderURI: String): Seq[String] = {
    aggregatedRelations.get(broaderURI).getOrElse(Seq.empty[String])
  }
  println("Narrower terms:")
  getNarrowerTerms(START_URI).foreach(println(_))
    
  // Step 3: recurse, until there are no more matches for "broader" categories
    
  println("Done.")

}