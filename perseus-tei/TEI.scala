/** A Scala script to convert the Perseus TEI text for Pliny's Natural History to .txt and .html.
  * 
  * Run the script using:
  * > scala TEI.scala
  * 
  * The results are in the results/ folder.
  */ 
import scala.xml.XML
import java.io.{ File, FileWriter }
import scala.xml.transform.RewriteRule
import scala.xml.Node
import scala.xml.NodeSeq
import scala.xml.Elem
import scala.xml.transform.RuleTransformer
import scala.xml.Text

// Parse XML
val xml = XML.loadFile("/home/simonr/Workspaces/pelagios/pelagios3-scripts/tei/Perseus_text_1999.02.0137.xml")

// Create output directories
val outputDir = new File("output")
if (!outputDir.exists)
  outputDir.mkdir()
  
val txtDir = new File(outputDir, "txt")
if (!txtDir.exists)
  txtDir.mkdir()

val htmlDir = new File(outputDir, "html")
if (!htmlDir.exists)
  htmlDir.mkdir()

// Transform XML to txt & HTML
val books = xml \\ "div1"
  
val indexWriter = new FileWriter(new File(htmlDir, "index.html"))
indexWriter.write("<html><head><title>Pliny - Natural History</title></head><body>");
  
books.foreach(book => {
  val bookNumber = (book \ "@n").text.toInt
    
  // We're only interested in books 3 - 6 (the geographical ones)
  if (bookNumber > 2 && bookNumber < 7) {
    indexWriter.write("<h1>Book " + bookNumber + "</h1>")
      
    (book \\ "div2").foreach(chapter => {        
      val removeNotes = new RewriteRule {
        override def transform(n: Node): NodeSeq = n match {
          case e: Elem if (e.label.equals("note")) => Text("")
          case e: Elem if (e.label.equals("head")) => Text(e.text + "\n\n")
          case e: Text => Text(e.text.replace("\n", " "))
          case n => n
        }
      }
        
      val cleaned = new RuleTransformer(removeNotes).transform(chapter).text.replaceAll(" +", " ").replace("\n ", "\n").trim().replace("- ", "")

      val filename = "book" + bookNumber + "_chapter" + (chapter \ "@n").text
        
      val plainTxtWriter = new FileWriter(new File(txtDir, filename + ".txt"))
      plainTxtWriter.write(cleaned)
      plainTxtWriter.flush()
      plainTxtWriter.close()
        
      indexWriter.write("<a href=\"" + filename + ".html\">" + filename + "</a><br/>")
        
      val html = "<html><head><meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\"></head><body>" + 
        cleaned.replace("\n", "<br/>") + "</body>"
        
      val htmlWriter = new FileWriter(new File(htmlDir, filename + ".html"))
      htmlWriter.write(html)
      htmlWriter.flush()
      htmlWriter.close()
   })
  }
})
  
  indexWriter.flush()
  indexWriter.close()
