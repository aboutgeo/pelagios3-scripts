/** A Scala script to convert the Perseus TEI text for Pliny's Natural History to .txt and .html.
  * 
  * Run the script using:
  * > scala TEI.scala
  * 
  * The results are in the results/ folder.
  */ 
import scala.collection.JavaConversions._
import scala.xml.XML
import java.io.{ File, FileWriter }
import scala.xml.transform.RewriteRule
import scala.xml.Node
import scala.xml.NodeSeq
import scala.xml.Elem
import scala.xml.transform.RuleTransformer
import scala.xml.Text

// Parse XML
val baseDir = new File("/home/simonr/Arbeitsfläche/pliny/txt")

val writer = new FileWriter("book3.txt");

val files = baseDir.listFiles.filter(_.getName.startsWith("book3")).map(file => {
  val number = (file.getName().substring(13, file.getName.indexOf("."))).toInt
  (file, number)
}).sortBy(_._2).foreach(tuple => {
  println(tuple._2)
  val text = scala.io.Source.fromFile(tuple._1).getLines.mkString("\n")
  writer.write(text + "\n\n")
})

writer.flush()
writer.close()
